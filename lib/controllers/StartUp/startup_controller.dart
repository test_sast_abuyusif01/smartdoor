import 'package:get/get.dart';

class StartupController extends GetxController {
  final isLoginLoading = false.obs;
  final isRegistrationLoading = false.obs;

  Future<void> handleLoginButtonPressed() async {
    isLoginLoading.value = true;
    await Future.delayed(const Duration(seconds: 1)); // Simulate a 3-second delay
    isLoginLoading.value = false;
    Get.toNamed('/login');
  }

  Future<void> handleRegisterButtonPressed() async {
    isRegistrationLoading.value = true;
    await Future.delayed(const Duration(seconds: 1)); // Simulate a 3-second delay
    isRegistrationLoading.value = false;
    Get.toNamed('/signUp');
  }
}
