// ignore_for_file: deprecated_member_use

import 'package:flutter/material.dart';
import 'package:get/get_state_manager/get_state_manager.dart';

class Tabs extends GetxController with SingleGetTickerProviderMixin {
  TabController? tabController;

  final List<Tab> myTabs = <Tab>[
    const Tab(text: 'All Doors'),
    const Tab(text: 'Manage Access'),
  ];

  @override
  void onInit() {
    tabController = TabController(length: 2, vsync: this);
    super.onInit();
  }

  @override
  void onClose() {
    tabController!.dispose();
    super.onClose();
  }
}
