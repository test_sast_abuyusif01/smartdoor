import 'package:get/get.dart';

class NavigationCount extends GetxController {
  RxInt count = 0.obs;
  get countValue => count.value;

  void setCount(count) {
    this.count.value = count;
  }
}
