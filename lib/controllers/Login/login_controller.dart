// ignore_for_file: use_build_context_synchronously

import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'dart:io';

import '../../constants/const_res.dart';

class User {
  final String email;
  final String password;
  final String token;
  final String role;
  final int userId;

  User({required this.role, required this.token, required this.email, required this.password, required this.userId});
}

class LoginController extends GetxController {
  final user = User(token: '', email: '', password: '', role: 'user', userId: 0).obs;

  void updateEmail({required String value}) {
    user.value = User(token: user.value.token, email: value, password: user.value.password, role: user.value.role, userId: user.value.userId);
  }

  void updatePassword({required String value}) {
    user.value = User(token: user.value.token, email: user.value.email, password: value, role: user.value.role, userId: user.value.userId);
  }

  void updateToken({required String value}) {
    user.value = User(token: value, email: user.value.email, password: user.value.password, role: user.value.role, userId: user.value.userId);
  }

  void updateRole({required String value}) {
    user.value = User(token: user.value.token, email: user.value.email, password: user.value.password, role: value, userId: user.value.userId);
  }

  void updateUserId({required int value}) {
    user.value = User(token: user.value.token, email: user.value.email, password: user.value.password, role: user.value.role, userId: value);
  }

  get getEmail => user.value.email;
  get getPassword => user.value.password;
  get getToken => user.value.token;
  get getRole => user.value.role;
  get getUserId => user.value.userId;

  Future<void> login(BuildContext context) async {
    final url = Uri.parse('$serverAddr:$serverPort/api/login');
    final body = '{"email": "${user.value.email}", "password": "${user.value.password}"}';

    try {
      HttpClient httpClient = HttpClient();
      httpClient.badCertificateCallback = (X509Certificate cert, String host, int port) => true; // Disables SSL verification

      HttpClientRequest request = await httpClient.postUrl(url);
      request.headers.set('content-type', 'application/json');
      request.add(utf8.encode(body));

      HttpClientResponse response = await request.close();
      String responseBody = await response.transform(utf8.decoder).join();
      Map<String, dynamic> jsonResponse = json.decode(responseBody);

      if (response.statusCode == 200) {
        //set token
        updateToken(value: jsonResponse['token']);
        updateRole(value: jsonResponse['role']);
        updateUserId(value: jsonResponse['userId']);
        ScaffoldMessenger.of(context).showSnackBar(
          const SnackBar(
            backgroundColor: Colors.green,
            content: Text(
              'Login Successful',
              style: TextStyle(
                color: Colors.white,
                fontSize: 16,
              ),
            ),
          ),
        );
        Get.toNamed('/doors');
      } else {
        ScaffoldMessenger.of(context).showSnackBar(
          const SnackBar(
            backgroundColor: Colors.red,
            content: Text(
              'Invalid Username or Password',
              style: TextStyle(
                color: Colors.white,
                fontSize: 16,
              ),
            ),
          ),
        );
      }
    } catch (e) {
      print(e.toString());
      ScaffoldMessenger.of(context).showSnackBar(
        SnackBar(
          backgroundColor: Colors.red,
          content: Text(
            e.toString(),
            style: const TextStyle(
              color: Colors.white,
              fontSize: 16,
            ),
          ),
        ),
      );
    }
  }
}
