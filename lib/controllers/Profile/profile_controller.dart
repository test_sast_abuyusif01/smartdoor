import 'dart:convert';
import 'dart:io';

import 'package:get/get.dart';
import 'package:flutter/material.dart';
import 'package:smartdoor/controllers/Login/login_controller.dart';

import '../../constants/const_res.dart';

class ProfileController extends GetxController {
  var isEditing = false.obs;

  var passwordController = TextEditingController();
  var emailController = TextEditingController();
  final LoginController loginController = Get.put(LoginController());

  // Toggle the editing state
  void toggleEditing() {
    isEditing.value = !isEditing.value;
  }

  Future<void> updateUserInfo(context) async {
    try {
      final url = Uri.parse('$serverAddr:$serverPort/api/updateUserInfo');

      final body = '{"email": "${emailController.text}", "password": "${passwordController.text}", "userId": ${loginController.getUserId} }';

      HttpClient httpClient = HttpClient();
      httpClient.badCertificateCallback = (X509Certificate cert, String host, int port) => true; // Disables SSL verification

      HttpClientRequest request = await httpClient.postUrl(url);
      request.headers.set('content-type', 'application/json');
      request.headers.set('x-access-token', '${loginController.getToken}');
      request.add(utf8.encode(body));

      HttpClientResponse response = await request.close();
      String responseBody = await response.transform(utf8.decoder).join();
      Map<String, dynamic> jsonResponse = json.decode(responseBody);

      if (response.statusCode == 200) {
        // setting the new values
        loginController.updateEmail(value: jsonResponse['email']);
        loginController.updateRole(value: jsonResponse['role']);
        loginController.updateUserId(value: jsonResponse['userId']);

        // let the update take effect on profile page as well
        setEmailText(jsonResponse['email']);
        setPasswordText(passwordController.text.toString());

        ScaffoldMessenger.of(context).showSnackBar(
          const SnackBar(
            backgroundColor: Colors.green,
            content: Text(
              'Update Successful',
              style: TextStyle(color: Colors.white),
            ),
          ),
        );
      } else {
        ScaffoldMessenger.of(context).showSnackBar(
          SnackBar(
            backgroundColor: Colors.red,
            content: Text(
              jsonResponse['message'],
              style: const TextStyle(color: Colors.white),
            ),
          ),
        );
        setEmailText(loginController.getEmail);
        setPasswordText(loginController.getPassword);
      }
    } catch (e) {
      ScaffoldMessenger.of(context).showSnackBar(
        const SnackBar(
          backgroundColor: Colors.red,
          content: Text(
            "Something went wrong",
            style: TextStyle(color: Colors.white),
          ),
        ),
      );
    }
  }

  void setEmailText(emailController) {
    this.emailController.text = emailController.toString();
    update();
  }

  void setPasswordText(passwordController) {
    this.passwordController.text = passwordController.toString();
    update();
  }
}
