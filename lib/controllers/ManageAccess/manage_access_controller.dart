import 'package:get/get.dart';

class ManageAccess extends GetxController {
  final List<RxBool> iconColors = List.generate(
    10,
    (index) => false.obs,
  );

  RxBool favorite = false.obs;
  RxString name = 'John'.obs;
  RxBool lock = false.obs;

  get isFavorite => favorite.value;
  get isLock => lock.value;
  get getName => name.value;

  void changeFavorite() {
    favorite.value = !favorite.value;
  }

  void changeLock(index) {
    iconColors[index].value = !iconColors[index].value;
  }
}
