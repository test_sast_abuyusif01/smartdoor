// ignore_for_file: avoid_print

import 'dart:convert';

import 'package:get/get.dart';
import 'package:http/http.dart';
import 'dart:io';
import '../Login/login_controller.dart';
import '../../constants/const_res.dart';

class DoorsController extends GetxController {
  List<RxBool> iconColors = [];
  RxBool lock = false.obs;
  var doorsList = [].obs;

  // this should be in manageAccessController
  get isLocked => lock.value;
  get doorListLength => doorsList.length;

  @override
  void onInit() {
    fetchDoors();
    super.onInit();
  }

  void updateIconColors(data) {
    iconColors = List.generate(data.length, (index) => false.obs);
  }

  // make a function that aake the incex, sleep for 10sec and change the color of the icon
  void changeIconColor(index, value) async {
    await Future.delayed(const Duration(seconds: 10));
    iconColors[index].value = value;
  }

  Future<bool> openDoor(index) async {
    try {
      final url = Uri.parse('$serverAddr:$serverPort/api/openDoor');
      final userId = Get.find<LoginController>().getUserId;
      final body = '{"doorId": "${doorsList[index]['doorId']}", "userId": "$userId"}';

      HttpClient httpClient = HttpClient();
      httpClient.badCertificateCallback = (X509Certificate cert, String host, int port) => true; // Disables SSL verification

      HttpClientRequest request = await httpClient.postUrl(url);
      request.headers.set('content-type', 'application/json');
      request.headers.set('x-access-token', '${Get.find<LoginController>().getToken}');

      request.add(utf8.encode(body));

      HttpClientResponse response = await request.close();
      String responseBody = await response.transform(utf8.decoder).join();
      Map<String, dynamic> jsonResponse = json.decode(responseBody);

      if (response.statusCode == 200) {
        iconColors[index].value = !iconColors[index].value;
        print(jsonResponse['message']);
        return true;
      } else {
        print('Error status code: ${response.statusCode}');
      }
    } catch (e) {
      print(e);
    }

    return false;
  }

  Future<bool> closeDoor(index) async {
    try {
      final url = Uri.parse('$serverAddr:$serverPort/api/closeDoor');
      final userId = Get.find<LoginController>().getUserId;
      final body = '{"doorId": "${doorsList[index]['doorId']}", "userId": "$userId"}';

      HttpClient httpClient = HttpClient();
      httpClient.badCertificateCallback = (X509Certificate cert, String host, int port) => true; // Disables SSL verification

      HttpClientRequest request = await httpClient.postUrl(url);
      request.headers.set('content-type', 'application/json');
      request.headers.set('x-access-token', '${Get.find<LoginController>().getToken}');
      request.add(utf8.encode(body));

      HttpClientResponse response = await request.close();
      String responseBody = await response.transform(utf8.decoder).join();
      Map<String, dynamic> jsonResponse = json.decode(responseBody);

      if (response.statusCode == 200) {
        iconColors[index].value = !iconColors[index].value;
        print(jsonResponse['message']);
        return true;
      } else {
        print('Error status code: ${response.statusCode}');
      }
    } catch (e) {
      print(e);
    }

    return false;
  }

  Future<void> fetchDoors() async {
    final userRole = Get.find<LoginController>().getRole;
    late String path;
    userRole == 'admin' ? path = 'getAllDoors' : path = 'getAllDoorsWithAccess';
    try {
      final url = Uri.parse('$serverAddr:$serverPort/api/$path');
      HttpClient httpClient = HttpClient();
      httpClient.badCertificateCallback = (X509Certificate cert, String host, int port) => true; // Disables SSL verification

      HttpClientRequest request = await httpClient.getUrl(url);
      request.headers.set('content-type', 'application/json');
      request.headers.set('x-access-token', '${Get.find<LoginController>().getToken}');

      HttpClientResponse response = await request.close();
      String responseBody = await response.transform(utf8.decoder).join();
      Map<String, dynamic> jsonResponse = json.decode(responseBody);

      if (response.statusCode == 200) {
        doorsList.value = jsonResponse['doors'];
        updateIconColors(doorsList);
      } else {
        print('Error status code: ${response.statusCode}');
      }
    } catch (e) {
      print(e);
    }
  }

  Future<String> getDoorPin(doorId) async {
    try {
      final url = Uri.parse('$serverAddr:$serverPort/api/getDoorPin?doorId=$doorId');

      HttpClient httpClient = HttpClient();
      httpClient.badCertificateCallback = (X509Certificate cert, String host, int port) => true; // Disables SSL verification

      HttpClientRequest request = await httpClient.getUrl(url); // Use getUrl for a GET request
      request.headers.set('content-type', 'application/json');
      request.headers.set('x-access-token', '${Get.find<LoginController>().getToken}');

      HttpClientResponse response = await request.close();
      String responseBody = await response.transform(utf8.decoder).join();
      Map<String, dynamic> jsonResponse = json.decode(responseBody);

      if (response.statusCode == 200) {
        return jsonResponse['doorPasscode'];
      } else {
        print('Error status code: ${response.statusCode}');
      }
    } catch (error) {
      print('An error occurred: $error');
    }
    return ''; // Return an appropriate default value here
  }
}
