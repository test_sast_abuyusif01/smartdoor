// ignore_for_file: use_build_context_synchronously
import 'dart:convert';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'dart:io';

import '../../constants/const_res.dart';

class User {
  final String email;
  final String password;
  final String role;

  User({required this.email, required this.password, required this.role});
}

class SignupController extends GetxController {
  final user = User(email: '', password: '', role: 'user').obs;

  void updateEmail(String value) {
    user.value = User(email: value, password: user.value.password, role: user.value.role);
  }

  void updatePassword(String value) {
    user.value = User(email: user.value.email, password: value, role: user.value.role);
  }

  get getEmail => user.value.email;
  get getPassword => user.value.password;

  Future<void> signup(BuildContext context) async {
    final url = Uri.parse('$serverAddr:$serverPort/api/register');
    final body = '{"email": "${user.value.email}", "password": "${user.value.password}", "role": "${user.value.role}"}';

    try {
      HttpClient httpClient = HttpClient();
      httpClient.badCertificateCallback = (X509Certificate cert, String host, int port) => true; // Disables SSL verification

      HttpClientRequest request = await httpClient.postUrl(url);
      request.headers.set('content-type', 'application/json');
      request.add(utf8.encode(body));

      HttpClientResponse response = await request.close();
      // String responseBody = await response.transform(utf8.decoder).join();

      if (response.statusCode == 200) {
        ScaffoldMessenger.of(context).showSnackBar(
          const SnackBar(
            backgroundColor: Colors.green,
            content: Text(
              'Registration Successful',
              style: TextStyle(
                color: Colors.white,
                fontSize: 16,
              ),
            ),
          ),
        );
        Get.toNamed('/doors');
      } else {
        ScaffoldMessenger.of(context).showSnackBar(
          const SnackBar(
            backgroundColor: Colors.red,
            content: Text(
              'Registration Failed',
              style: TextStyle(
                color: Colors.white,
                fontSize: 16,
              ),
            ),
          ),
        );
      }
    } catch (e) {
      ScaffoldMessenger.of(context).showSnackBar(
        SnackBar(
          backgroundColor: Colors.red,
          content: Text(
            e.toString(),
            style: const TextStyle(
              color: Colors.white,
              fontSize: 16,
            ),
          ),
        ),
      );
    }
  }
}
