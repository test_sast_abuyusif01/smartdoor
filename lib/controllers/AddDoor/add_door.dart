import 'package:get/get.dart';

class AddDoorController extends GetxController {
  var passcode = ''.obs;
  void setPassCode(String value) => passcode.value = value;
}
