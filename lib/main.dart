import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:smartdoor/screens/AddDoor/start.dart';
import 'package:smartdoor/screens/Doors/ManageAccess/AllUsers/start.dart';
import 'package:smartdoor/screens/Doors/start.dart';
import 'package:smartdoor/screens/Doors/ManageAccess/manage_doors.dart';
import 'package:smartdoor/screens/Login/start.dart';
import 'package:smartdoor/screens/Profile/start.dart';
import 'package:smartdoor/screens/QrScan/start.dart';
import 'package:smartdoor/screens/SignUp/start.dart';
import 'package:smartdoor/screens/StartUp/start.dart';
import 'constants/routes.dart';

void main() {
  runApp(const MyApp());
}

class MyApp extends StatelessWidget {
  const MyApp({super.key});

  @override
  Widget build(BuildContext context) {
    return GetMaterialApp(
      theme: ThemeData(
        colorScheme: ColorScheme.fromSeed(seedColor: Colors.white),
        useMaterial3: true,
        visualDensity: VisualDensity.adaptivePlatformDensity,
      ),
      debugShowCheckedModeBanner: false,
      home: StartupPage(),
      getPages: [
        GetPage(name: Routes.qrscan, page: () => const QrScan()),
        GetPage(name: Routes.startUp, page: () => StartupPage()),
        GetPage(name: Routes.login, page: () => Login()),
        GetPage(name: Routes.signUp, page: () => const SignUp()),
        GetPage(name: Routes.manageAccess, page: () => const ManageDoors()),
        GetPage(name: Routes.doors, page: () => const Doors()),
        GetPage(name: Routes.profile, page: () => Profile()),
        GetPage(name: Routes.addDoor, page: () => const AddDoor()),
        GetPage(name: Routes.allCurrentAccess, page: () => const AllCurrentAccess())
      ],
    );
  }
}
