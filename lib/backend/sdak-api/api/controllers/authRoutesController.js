const { User } = require("../../models");
const config = require("../config/authConfig");
var jwt = require("jsonwebtoken");
var bcrypt = require("bcryptjs");
const { validateAlphanumeric } = require("./utils/validator");


/**
 * 
 * @param {*} req 
 * @param {*} res 
 * @returns void
 * 
 * route to register a user
 */
exports.signup = (req, res) => {

    if (!validateAlphanumeric(req.body.email.toString(), req.body.password.toString())) {
        return res.status(400).send({ message: "Invalid user input", })
    }
    
    User.create({
        email: req.body.email,
        password: bcrypt.hashSync(req.body.password, 8),
        role: "user"
    }).then(() => {
        res.send({ message: "User registered successfully!" });
    });

};


/**
 * 
 * @param {*} req 
 * @param {*} res
 * @returns void
 * 
 * route to login a user
 */
exports.signin = (req, res) => {

    if (!validateAlphanumeric(req.body.email.toString(), req.body.password.toString())) {
        return res.status(400).send({ message: "Invalid user input", })
    }
    User.findOne({
        where: { email: req.body.email }
    }).then(user => {
        if (!user) {
            return res.status(404).send({ message: "Invalid Username or Password!" });
        }

        var passwordIsValid = bcrypt.compareSync(
            req.body.password,
            user.password
        );

        if (!passwordIsValid) {
            return res.status(401).send({
                message: "Invalid Username or Password!"
            });
        }

        var token = jwt.sign({ userId: user.userId }, config.secret, {
            expiresIn: 86400 // 24 hours
        });

        res.status(200).send({
            userId: user.userId,
            role: user.role,
            email: user.email,
            token: token
        });
    }).catch(err => {
        res.status(500).send({ message: err.message });
    });
};
