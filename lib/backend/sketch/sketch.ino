/*Dev Log
1. Use HiveMq as MQTT broker
2. Simple IoT system to open/off solenoid door
3. WifiManager lib for wifi webclient config
4. 4/2/2023: Add delay for relay input HIGH (Door open), buzz alarm/send warning message to HiveMQ if relay input HIGH (door is still open) after delay
5. 10/2/2023: Add buzzer, manual switch + 5 seconds delay, secure connection
6. Remove active buzzer, replace with passive and directly connect to relay = buzz on each time door open + off when door close
7. Add support for magnetic contact switch = indicate open/close door
8. Get nodeMCU UID to assign as door UID
9. Fix door logic = door always close and auto close after 10 seconds, add 2 UID to distinguished 2 doors ID-DCS= e101c2, , ID-DLIS=4a6c4d , 3 prototype: 8984b0
*/

#include <ESP8266WiFi.h>
#include <PubSubClient.h>


// const for door pin, uuid and buzzer
static String doorUID = String(ESP.getChipId(), HEX);
const int relay1 = 5, relay2 = 4, buzzer = 14, swtch = 12, rstSwitch = 13, deskSW = 2, mqtt_port = 8883;
int manualSWCurrent, manualSWLast, deskSWCurrent, deskSWLast, rstStateCurrent, rstStateLast;

// login creds for mqtt broker
const char * mqtt_ip = "192.168.146.127";
const char * mqtt_username = "user";
const char * mqtt_password = "1234";

WiFiClientSecure wifiClient;
PubSubClient mqttClient(wifiClient);

// MQTT publising as string
void publishMessage(const char * topic, String payload, boolean retained) {
  if (mqttClient.publish(topic, payload.c_str(), true))
    Serial.println("Message publised [" + String(topic) + "]: " + payload);
}

// a function to connect to wifi by supplying ssid and password
void connectToWifi(const char * ssid, const char * password) {
  Serial.print("Connecting to ");
  Serial.println(ssid);
  WiFi.begin(ssid, password);
  while (WiFi.status() != WL_CONNECTED) {
    delay(500);
    Serial.print(".");
  }

  Serial.print("\nWiFi connected, IP address: ");
  Serial.println(WiFi.localIP());
  wifiClient.setInsecure();
}

// a function to connect to mqtt broker by supplying broker ip, port, username, password, client-id, topic-name and root certificate
void connectToMqttBroker(const char * mqttBrokerIp, int mqttBrokerPort, const char * mqttUsername, const char * mqttPassword) {
  String topicName = "/door/" + doorUID;
  Serial.print("Connecting to MQTT Broker: ");
  Serial.println(mqttBrokerIp);
  mqttClient.setServer(mqttBrokerIp, mqttBrokerPort);
  mqttClient.setCallback(callback);
  while (!mqttClient.connected()) {
    if (mqttClient.connect(doorUID.c_str(), mqttUsername, mqttPassword)) {
      Serial.println("Connected to MQTT Broker");
      mqttClient.subscribe(topicName.c_str());
    } else {
      Serial.print("Failed to connect to MQTT Broker, state: ");
      Serial.println(mqttClient.state());
      delay(2000);
    }
  }
}

void callback(char * topic, byte * payload, unsigned int length) {
  String incommingMessage = "";
  for (int i = 0; i < length; i++) {
    incommingMessage += (char) payload[i];
  }

  Serial.println("HiveMQ Incomming Message[" + String(topic) + "]: " + incommingMessage);
  if (incommingMessage.equals("1")) { // relay HIGH = OPEN, LOW = CLOSE
    publishMessage(doorUID.c_str(), String("OPEN"), true);
    pinMode(relay1, HIGH);
    pinMode(relay2, HIGH);
    digitalWrite(buzzer, HIGH);
    delay(3000);
    digitalWrite(buzzer, LOW);
    delay(7000);
    pinMode(relay1, LOW);
    pinMode(relay2, LOW);
    publishMessage(doorUID.c_str(), String("CLOSED"), true);
  }
}

// MQTT callback

void onManualSwitch() {
  const char * topic = "Door";
  // Open door
  pinMode(relay1, HIGH);
  pinMode(relay2, HIGH);
  digitalWrite(buzzer, HIGH);
  Serial.println("Manual Switch ON: Door Open");
  publishMessage(topic, String("Front Desk Switch ON: Door Open"), true);
  delay(3000);
  digitalWrite(buzzer, LOW);

  // Close door after 10 seconds
  delay(10000);
  pinMode(relay1, LOW);
  pinMode(relay2, LOW);
  Serial.println("Manual Switch OFF: Door Close");
  publishMessage(topic, String("Front Desk Switch OFF: Door Close"), true);
}

/*****Setup && Loop*****/
void setup() {
  Serial.begin(115200);
  while (!Serial)
    delay(1);
  doorUID = String(ESP.getChipId(), HEX);
  Serial.printf("\n ESP8266 Chip id = %08X\n", doorUID); // get nodeMCU UID
  pinMode(relay1, OUTPUT);
  pinMode(relay2, OUTPUT);
  pinMode(buzzer, OUTPUT);
  pinMode(swtch, INPUT_PULLUP);
  pinMode(rstSwitch, INPUT_PULLUP);
  pinMode(deskSW, INPUT_PULLUP);
  connectToWifi("free and open if u nigga v2", "freeandopen1234");

  // MQTT Init
  mqttClient.setServer(mqtt_ip, mqtt_port);
  mqttClient.setCallback(callback);
}

void loop() {
  pinMode(relay1, LOW);
  pinMode(relay2, LOW);

  // init reset button to restart nodemcu
  rstStateLast = rstStateCurrent;
  rstStateCurrent = digitalRead(rstSwitch);
  if (rstStateLast == HIGH && rstStateCurrent == LOW) {
    Serial.printf("Restart NodeMCU..");
    ESP.restart();
  }
  // Front Desk Switch: Open door for 10 seconds.
  deskSWLast = deskSWCurrent;
  deskSWCurrent = digitalRead(deskSW);
  if (deskSWLast == HIGH && deskSWCurrent == LOW) {
    onManualSwitch();
  }
  // Manual Switch: Open door for 10 seconds.
  manualSWLast = manualSWCurrent;
  manualSWCurrent = digitalRead(swtch);
  if (manualSWLast == HIGH && manualSWCurrent == LOW) {
    onManualSwitch();
  }
  // Client listener
  if (!mqttClient.connected())
    connectToMqttBroker(mqtt_ip, mqtt_port, mqtt_username, mqtt_password);
  mqttClient.loop();
}