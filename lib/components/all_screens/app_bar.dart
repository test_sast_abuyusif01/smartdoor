import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:smartdoor/screens/Login/start.dart';
import '../../constants/app_colors.dart';

appBar(text, icon1, icon2, buttom, {visibility = true}) {
  return AppBar(
    title: Text(
      text.toString(),
      style: const TextStyle(
        fontWeight: FontWeight.w500,
        fontSize: 30,
        color: Colors.white70,
      ),
    ),
    centerTitle: true,
    backgroundColor: appBarcolor.withAlpha(170),
    leading: Container(
      padding: const EdgeInsets.only(
        left: 15,
        top: 5,
      ),
      child: IconButton(
        icon: icon1,
        onPressed: () {
          Get.until((route) => Get.currentRoute == '/doors');
        },
      ),
    ),
    bottom: buttom,
    elevation: 5,
    actions: [
      Container(
        padding: const EdgeInsets.only(
          right: 20,
          top: 5,
        ),
        child: Visibility(
          visible: visibility,
          child: IconButton(
            icon: icon2,
            onPressed: () {},
          ),
        ),
      )
    ],
  );
}
