// ignore_for_file: avoid_print

import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:google_nav_bar/google_nav_bar.dart';
import 'package:smartdoor/screens/AddDoor/start.dart';
import 'package:smartdoor/screens/Doors/start.dart';
import 'package:smartdoor/screens/Profile/start.dart';
import 'package:smartdoor/screens/QrScan/start.dart';
import '../../constants/app_colors.dart';

navBar(count) {
  return Container(
    decoration: const BoxDecoration(
      color: Colors.white70,
      boxShadow: [
        BoxShadow(
          blurRadius: 5,
          color: Colors.black12,
        ),
      ],
    ),
    child: Padding(
      padding: const EdgeInsets.symmetric(horizontal: 0, vertical: 0),
      child: GNav(
        // debug: true,
        gap: 0,
        activeColor: activeNavIconColor.withAlpha(170),
        color: inactiveNavIconColor,
        hoverColor: inactiveNavIconColor.withAlpha(100),
        selectedIndex: count,
        onTabChange: (value) => {
          if (value == 0) {Get.to(() => const Doors())},
          if (value == 1) {Get.to(() => const QrScan())},
          if (value == 2) {Get.to(() => const AddDoor())},
          if (value == 3) {Get.to(() => Profile())}
        },
        tabs: const [
          GButton(
            icon: Icons.door_sliding_outlined,
            text: 'Doors',
          ),
          GButton(
            icon: Icons.qr_code_scanner_sharp,
            text: 'Qr Scan',
          ),
          GButton(
            icon: Icons.add,
            text: 'Add Doors',
          ),
          GButton(
            icon: Icons.person,
            text: 'Profile',
          ),
        ],
      ),
    ),
  );
}
