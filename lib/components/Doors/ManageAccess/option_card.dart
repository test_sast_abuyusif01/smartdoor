import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:qr_flutter/qr_flutter.dart';
import 'package:quickalert/quickalert.dart';
import 'package:smartdoor/controllers/Doors/doors_controller.dart';

import '../../../constants/app_colors.dart';

oCard(text, icon, doorId, doorName, context) {
  final DoorsController doorController = Get.put(DoorsController());
  return Container(
    decoration: BoxDecoration(
      borderRadius: BorderRadius.circular(10),
      color: Colors.white,
    ),
    child: Column(
      mainAxisAlignment: MainAxisAlignment.center,
      children: [
        Container(
          padding: const EdgeInsets.all(10),
          child: icon,
        ),
        Container(
          height: 50,
          decoration: BoxDecoration(color: activeNavIconColor.withAlpha(240), borderRadius: const BorderRadius.only(bottomRight: Radius.circular(10), bottomLeft: Radius.circular(10))),
          padding: const EdgeInsets.all(10),
          child: TextButtonTheme(
            data: TextButtonThemeData(
              style: TextButton.styleFrom(
                foregroundColor: Colors.white,
                textStyle: const TextStyle(fontSize: 20),
              ),
            ),
            child: TextButton(
              onPressed: () async {
                if (text == "View Qr") {
                  showDialog(
                    context: context,
                    builder: (BuildContext context) {
                      return AlertDialog(
                        title: Center(child: Text(doorName.toString())),
                        content: SizedBox(
                          width: 200,
                          height: 250,
                          child: Column(
                            mainAxisAlignment: MainAxisAlignment.center,
                            crossAxisAlignment: CrossAxisAlignment.center,
                            children: <Widget>[
                              QrImageView(
                                data: doorController.getDoorPin(doorId).toString(),
                                version: QrVersions.auto,
                                size: 150.0,
                              ),
                              const SizedBox(height: 10),
                              const Text('Scan this QR code'),
                            ],
                          ),
                        ),
                        actions: [
                          TextButton(
                            onPressed: () {
                              Navigator.pop(context); // Close the dialog
                            },
                            child: const Text('Close'),
                          ),
                        ],
                      );
                    },
                  ); // push notification
                } else if (text == "View Pin") {
                  QuickAlert.show(
                    context: context,
                    type: QuickAlertType.info,
                    text: 'Navigate to add door pin page and add the pin for this door',
                    // instead of this text, we need to show the pin
                    title: 'Pin: ${await doorController.getDoorPin(doorId)}',
                  );
                } else if (text == "Access") {
                  Get.toNamed("/allCurrentAccess");
                }
              },
              style: TextButton.styleFrom(
                fixedSize: const Size(150, 0),
                backgroundColor: activeNavIconColor.withAlpha(170),
                shape: const RoundedRectangleBorder(
                  borderRadius: BorderRadius.all(
                    Radius.circular(10),
                  ),
                ),
              ),
              child: Text(text),
            ),
          ),
        )
      ],
    ),
  );
}
