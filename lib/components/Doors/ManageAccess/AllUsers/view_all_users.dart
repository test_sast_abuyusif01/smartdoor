import 'package:get/get.dart';
import 'package:quickalert/quickalert.dart';
import 'package:flutter/material.dart';
import 'package:smartdoor/constants/app_colors.dart';
import 'package:smartdoor/controllers/ManageAccess/manage_access_controller.dart';

doorList({
  required String imageUrl,
  required String title,
  required String subtitle,
  required Icon icon,
  required String route,
}) {
  return ListView.builder(
    scrollDirection: Axis.vertical,
    itemCount: 10,
    itemBuilder: (context, index) {
      return pCard(imageUrl, title, subtitle, icon, route, context, index);
    },
  );
}

pCard(String imageUrl, String title, String subtitle, Icon icon, String route, context, index) {
  final ManageAccess manageAccess = Get.put(ManageAccess());
  return GestureDetector(
    onDoubleTap: () => {},
    onLongPress: () => {},
    child: Card(
      elevation: 2.5,
      margin: const EdgeInsets.symmetric(horizontal: 10, vertical: 6),
      child: Container(
        decoration: const BoxDecoration(color: Colors.white70),
        child: ListTile(
          contentPadding: const EdgeInsets.symmetric(horizontal: 15.0, vertical: 6.0),
          leading: Container(
            padding: const EdgeInsets.only(right: 12.0, top: 6.0),
            decoration: const BoxDecoration(border: Border(right: BorderSide(width: 1.0, color: Colors.white24))),
            child: Image.network(
              imageUrl,
              color: activeNavIconColor.withAlpha(170),
              height: 35.0,
            ),
          ),
          title: Text(
            title.toString(),
            style: TextStyle(color: Colors.black.withAlpha(100), fontSize: 20),
          ),
          subtitle: Row(
            children: [Text(subtitle.toString(), style: const TextStyle(color: Colors.black))],
          ),
          isThreeLine: true,
          trailing: Obx(
            () => IconButton(
              icon: icon,
              color: manageAccess.iconColors[index].value ? inactiveNavIconColor : Colors.greenAccent,
              onPressed: () {
                !manageAccess.iconColors[index].value
                    ? QuickAlert.show(
                        context: context,
                        type: QuickAlertType.confirm,
                        text: 'Do you want to revoke user access',
                        confirmBtnText: 'Yes',
                        cancelBtnText: 'No',
                        confirmBtnColor: Colors.red.shade400,
                        onConfirmBtnTap: () => {
                          manageAccess.changeLock(index),
                          Navigator.pop(context),
                        },
                      )
                    : QuickAlert.show(
                        context: context,
                        type: QuickAlertType.confirm,
                        text: 'Do you want to grant user access',
                        confirmBtnText: 'Yes',
                        cancelBtnText: 'No',
                        confirmBtnColor: Colors.green,
                        onConfirmBtnTap: () => {
                          manageAccess.changeLock(index),
                          Navigator.pop(context),
                        },
                      );
              },
            ),
          ),
        ),
      ),
    ),
  );
}
