import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:smartdoor/constants/app_colors.dart';

currentAccess(doorId, location, code) {
  return Expanded(
    flex: 2,
    child: GridView.builder(
      padding: const EdgeInsets.all(10),
      gridDelegate: const SliverGridDelegateWithFixedCrossAxisCount(
        crossAxisCount: 2, // You can adjust this number to fit your needs
        mainAxisSpacing: 10,
        crossAxisSpacing: 10,
      ),
      itemBuilder: (BuildContext context, int index) {
        return Card(
          child: Padding(
            padding: const EdgeInsets.all(8.0),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                const Icon(Icons.door_back_door_outlined),
                Text(
                  doorId.toString(),
                  style: const TextStyle(
                    fontWeight: FontWeight.bold,
                    fontSize: 18,
                  ),
                ),
                Text(
                  location.toString(),
                  style: const TextStyle(
                    color: Colors.grey,
                    fontSize: 16,
                  ),
                ),
                Text(
                  code.toString(),
                  style: const TextStyle(
                    color: Colors.grey,
                    fontSize: 16,
                  ),
                ),
                const SizedBox(height: 30),
                Padding(
                  padding: const EdgeInsets.only(top: 10),
                  child: GestureDetector(
                    onTap: () => {Get.toNamed("/allCurrentAccess")},
                    child: Container(
                      padding: const EdgeInsets.all(8),
                      width: double.infinity,
                      height: 40,
                      decoration: BoxDecoration(
                        color: activeNavIconColor,
                        borderRadius: BorderRadius.circular(10),
                      ),
                      child: const Center(
                        child: Text(
                          "View all subscribers",
                          style: TextStyle(
                            color: Colors.white,
                            fontSize: 15,
                          ),
                        ),
                      ),
                    ),
                  ),
                )
              ],
            ),
          ),
        );
      },
      itemCount: 5,
      scrollDirection: Axis.vertical, // This will make the gridview layout row-wise
    ),
  );
}
