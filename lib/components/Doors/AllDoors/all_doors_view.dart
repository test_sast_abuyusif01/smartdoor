import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:quickalert/models/quickalert_type.dart';
import 'package:quickalert/widgets/quickalert_dialog.dart';
import 'package:smartdoor/constants/app_colors.dart';
import 'package:smartdoor/controllers/Login/login_controller.dart';

import '../../../controllers/Doors/doors_controller.dart';

doorList({required String route, required Icon icon}) {
  final homePageController = Get.find<DoorsController>();

  return Obx(
    () => ListView.builder(
      scrollDirection: Axis.vertical,
      itemCount: homePageController.doorsList.length,
      itemBuilder: (context, index) {
        final door = homePageController.doorsList[index];
        final role = Get.find<LoginController>().getRole;
        return pCard(
          icon: icon,
          title: role == 'admin' ? door['doorName'] : door['door']['doorName'],
          subtitle: role == 'admin' ? door['doorLocation'] + "\nOpen all day" : door['door']['doorLocation'] + "\nOpen all day",
          route: route,
          context: context,
          index: index,
        );
      },
    ),
  );
}

pCard({required Icon icon, required String title, required String subtitle, required String route, required context, required int index}) {
  final doorController = Get.find<DoorsController>();
  // final door = doorController.doorsList[index];

  return GestureDetector(
    onTap: () => {},
    child: Card(
      elevation: 2.5,
      margin: const EdgeInsets.symmetric(horizontal: 10, vertical: 6),
      child: Container(
        decoration: const BoxDecoration(color: Colors.white70),
        child: ListTile(
          contentPadding: const EdgeInsets.symmetric(horizontal: 15.0, vertical: 6.0),
          leading: Container(
            padding: const EdgeInsets.only(right: 12.0, top: 6.0),
            decoration: const BoxDecoration(border: Border(right: BorderSide(width: 1.0, color: Colors.white24))),
            child: Icon(
              Icons.meeting_room_outlined,
              color: activeNavIconColor.withAlpha(170),
              size: 35.0,
            ),
          ),
          title: Text(
            title.toString(),
            style: TextStyle(color: Colors.black.withAlpha(100), fontSize: 20),
          ),
          subtitle: Row(
            children: [Text(subtitle.toString(), style: const TextStyle(color: Colors.black))],
          ),
          isThreeLine: true,
          trailing: Obx(
            () => IconButton(
              icon: icon,
              color: doorController.iconColors[index].value ? Colors.greenAccent : inactiveNavIconColor,
              onPressed: () {
                if (route == "0") {
                  if (doorController.iconColors[index].value) {
                    QuickAlert.show(
                      context: context,
                      type: QuickAlertType.confirm,
                      text: 'Do you want to close the door?',
                      confirmBtnText: 'Yes',
                      cancelBtnText: 'No',
                      confirmBtnColor: Colors.red,
                      onConfirmBtnTap: () async {
                        doorController.closeDoor(index);
                        Navigator.pop(context);
                      },
                    ).then((value) => {});
                  } else {
                    QuickAlert.show(
                      context: context,
                      type: QuickAlertType.confirm,
                      text: 'Do you want to open the door?',
                      confirmBtnText: 'Yes',
                      cancelBtnText: 'No',
                      confirmBtnColor: Colors.green,
                      onConfirmBtnTap: () async {
                        doorController.openDoor(index);
                        doorController.changeIconColor(index, doorController.iconColors[index].value);
                        Navigator.pop(context);
                      },
                    ).then((value) => {});
                  }
                } else {
                  Get.toNamed(route, arguments: {
                    'doorId': doorController.doorsList[index]['doorId'],
                    'doorName': doorController.doorsList[index]['doorName'],
                    'doorLocation': doorController.doorsList[index]['doorLocation'],
                    'doorCode': doorController.doorsList[index]['doorCode'],
                  });
                }
              },
            ),
          ),
        ),
      ),
    ),
  );
}
