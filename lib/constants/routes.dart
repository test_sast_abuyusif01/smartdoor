class Routes {
  static String qrscan = '/qrscan';
  static String doors = '/doors';
  static String addDoor = '/addDoor';
  static String profile = '/profile';
  static String manageAccess = '/manageAccess';
  static String allCurrentAccess = '/allCurrentAccess';
  static String login = '/login';
  static String signUp = '/signUp';
  static String startUp = '/startUp';
}
