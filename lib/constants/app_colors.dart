import 'dart:ui';

// const Color activeNavIconColor = Color(0xFF388CBB);
const Color activeNavIconColor = Color(0xFF008B88);
// const Color appBarcolor = Color(0xFF388CBB);
const Color appBarcolor = Color(0xFF008B88);
const Color inactiveNavIconColor = Color(0xFFB0B0B0);
const Color defaultTextColor = Color(0xFFB0B0B0);
const Color backgroundColor = Color(0xFF1E1E1E);
