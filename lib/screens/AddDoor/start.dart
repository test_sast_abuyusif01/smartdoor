import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:smartdoor/constants/app_colors.dart';
import 'package:smartdoor/controllers/AddDoor/add_door.dart';

import '../../components/all_screens/app_bar.dart';
import '../../components/all_screens/bottom_nav.dart';
import '../../controllers/Navigation/tab_view_controller.dart';

class AddDoor extends StatelessWidget {
  const AddDoor({super.key});

  @override
  Widget build(BuildContext context) {
    // ignore: unused_local_variable
    final Tabs tabs = Get.put(Tabs());
    final AddDoorController addDoorController = Get.put(AddDoorController());
    return Scaffold(
      appBar: appBar(
        'Add Door',
        const Icon(
          Icons.arrow_back,
          color: Colors.white70,
          size: 30,
        ),
        const Icon(
          Icons.search,
          color: Colors.white70,
          size: 30,
        ),
        PreferredSize(
          preferredSize: const Size(25, 25),
          child: Container(),
        ),
      ),
      bottomNavigationBar: navBar(2),
      body: Scaffold(
        body: Center(
          child: Padding(
            padding: const EdgeInsets.all(16.0),
            child: Column(
              mainAxisSize: MainAxisSize.min,
              children: [
                Container(
                  decoration: BoxDecoration(
                    gradient: LinearGradient(
                      begin: Alignment.topLeft,
                      end: Alignment.bottomRight,
                      colors: [
                        activeNavIconColor.withAlpha(100),
                        activeNavIconColor.withAlpha(120),
                        activeNavIconColor.withAlpha(160),
                      ],
                    ),
                  ),
                  child: TextFormField(
                    onChanged: (value) => addDoorController.setPassCode(value),
                    // obscureText: true,
                    decoration: InputDecoration(
                      hintText: "Enter Door Passcode",
                      prefixIcon: const Icon(
                        Icons.lock,
                        color: Colors.white70,
                      ),
                      enabledBorder: OutlineInputBorder(
                        borderSide: BorderSide.none,
                        borderRadius: BorderRadius.circular(10.0),
                      ),
                      focusedBorder: OutlineInputBorder(
                        borderSide: BorderSide.none,
                        borderRadius: BorderRadius.circular(10.0),
                      ),
                    ),
                  ),
                ),
                const SizedBox(height: 20.0),
                ElevatedButton(
                  onPressed: () {},
                  style: ElevatedButton.styleFrom(
                    foregroundColor: Colors.white,
                    backgroundColor: activeNavIconColor,
                    fixedSize: const Size(200, 45),
                    elevation: 20,
                    shape: RoundedRectangleBorder(
                      borderRadius: BorderRadius.circular(8.0),
                    ),
                  ),
                  child: const Text(
                    'Submit',
                    style: TextStyle(color: Colors.white70),
                  ),
                ),
              ],
            ),
          ),
        ),
      ),
    );
  }
}
