// ignore_for_file: library_private_types_in_public_api, avoid_print

import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:flutter/services.dart';
import 'package:smartdoor/constants/app_colors.dart';
import '../../components/all_screens/app_bar.dart';
import '../../components/all_screens/bottom_nav.dart';
import '../../controllers/Navigation/tab_view_controller.dart';
import 'package:qr_code_scanner/qr_code_scanner.dart';

class QrScan extends StatefulWidget {
  const QrScan({super.key});
  @override
  _QrScanState createState() => _QrScanState();
}

class _QrScanState extends State<QrScan> {
  final GlobalKey qrKey = GlobalKey(debugLabel: 'QR');
  QRViewController? controller;
  String scannedCode = '';
  @override
  Widget build(BuildContext context) {
    // ignore: unused_local_variable
    final Tabs tabs = Get.put(Tabs());
    return Scaffold(
      appBar: appBar(
        'Qr Scan',
        const Icon(
          Icons.arrow_back,
          color: Colors.white70,
          size: 30,
        ),
        const Icon(
          Icons.search,
          color: Colors.white70,
          size: 30,
        ),
        PreferredSize(
          preferredSize: const Size(25, 25),
          child: Container(),
        ),
      ),
      bottomNavigationBar: navBar(1),
      body: Center(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            Expanded(
              flex: 4,
              child: Container(
                color: inactiveNavIconColor,
                child: QRView(
                  key: qrKey,
                  onQRViewCreated: _onQRViewCreated,
                ),
              ),
            ),
            Expanded(
              flex: 1,
              child: Container(
                color: activeNavIconColor,
                child: const Center(
                  child: Text(
                    'Scan the QR code',
                    style: TextStyle(fontSize: 20, color: Colors.white),
                  ),
                ),
              ),
            ),
            Expanded(
              flex: 1,
              child: Container(
                padding: const EdgeInsets.all(10),
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: [
                    Expanded(
                      child: Text(
                        scannedCode,
                        style:
                            const TextStyle(fontSize: 16, color: Colors.black),
                      ),
                    ),
                    IconButton(
                      icon: const Icon(Icons.copy),
                      onPressed: () {
                        Clipboard.setData(ClipboardData(text: scannedCode));
                        ScaffoldMessenger.of(context).showSnackBar(
                          const SnackBar(content: Text('Copied to clipboard')),
                        );
                      },
                    ),
                  ],
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }

  void _onQRViewCreated(QRViewController controller) {
    this.controller = controller;
    controller.scannedDataStream.listen((scanData) {
      if (isValidQRCode(scanData.code)) {
        setState(() {
          scannedCode = scanData.code!;
        });
      }
    });
  }

  bool isValidQRCode(String? code) {
    // Add your validation logic here
    // For example, check if the code starts with 'http' or 'https'
    return code!.startsWith('http') || code.startsWith('https');
  }

  @override
  void dispose() {
    controller?.dispose();
    super.dispose();
  }
}


// https://google.com