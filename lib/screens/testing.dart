import 'package:flutter/material.dart';
import 'package:get/get.dart';
import '../components/all_screens/app_bar.dart';
import '../components/all_screens/bottom_nav.dart';
import '../controllers/Navigation/tab_view_controller.dart';

class Test extends StatelessWidget {
  const Test({super.key});

  @override
  Widget build(BuildContext context) {
    final Tabs tabs = Get.put(Tabs());
    return Scaffold(
      appBar: appBar(
        'Smart Door',
        const Icon(
          Icons.arrow_back,
          color: Colors.white70,
          size: 30,
        ),
        const Icon(
          Icons.search,
          color: Colors.white70,
          size: 30,
        ),
        TabBar(
          indicatorColor: Colors.black38,
          indicatorWeight: 3,
          labelColor: Colors.black,
          controller: tabs.tabController,
          tabs: tabs.myTabs,
        ),
      ),
      bottomNavigationBar: navBar(0),

      // body:
    );
  }
}
