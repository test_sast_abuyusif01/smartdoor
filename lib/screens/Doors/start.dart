// ignore_for_file: avoid_print, prefer_const_constructors, unused_local_variable

import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:smartdoor/components/all_screens/app_bar.dart';
import 'package:smartdoor/components/all_screens/bottom_nav.dart';
import 'package:smartdoor/constants/app_colors.dart';
import 'package:smartdoor/controllers/Doors/doors_controller.dart';

import '../../components/Doors/AllDoors/all_doors_view.dart';
import '../../controllers/Navigation/tab_view_controller.dart';

class Doors extends StatelessWidget {
  const Doors({super.key});

  @override
  Widget build(BuildContext context) {
    final Tabs tabs = Get.put(Tabs());
    DoorsController doorsController = Get.put(DoorsController());
    doorsController.fetchDoors();

    return Scaffold(
      appBar: appBar(
        'Smart Door',
        Icon(
          Icons.arrow_back,
          color: Colors.white70,
          size: 30,
        ),
        Icon(
          Icons.search,
          color: Colors.white70,
          size: 30,
        ),
        TabBar(
          indicatorColor: Colors.black38,
          indicatorWeight: 3,
          labelStyle: TextStyle(
            fontSize: 20,
            color: activeNavIconColor,
          ),
          labelColor: Colors.black,
          controller: tabs.tabController,
          tabs: tabs.myTabs,
        ),
      ),
      bottomNavigationBar: navBar(0),
      body: TabBarView(
        controller: tabs.tabController,
        children: [
          doorList(
            route: "0", // tryna be smart lolz,
            icon: Icon(
              Icons.vpn_key_off_outlined,
              size: 35.0,
            ),
          ),
          doorList(
            route: '/manageAccess',
            icon: const Icon(
              Icons.settings,
              color: Colors.greenAccent,
              size: 35.0,
            ),
          ),
        ],
      ),
      // body: Container(
      //   child: doorList(
      //     route: "0", // tryna be smart lolz,
      //     icon: Icon(
      //       Icons.vpn_key_off_outlined,
      //       size: 35.0,
      //     ),
      //   ),
      // ),
    );
  }
}
