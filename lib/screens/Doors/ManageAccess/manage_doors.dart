// ignore_for_file: deprecated_member_use

import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:smartdoor/components/Doors/ManageAccess/option_card.dart';
import '../../../components/all_screens/app_bar.dart';
import '../../../components/all_screens/bottom_nav.dart';
import '../../../components/Doors/ManageAccess/current_access.dart';
import '../../../constants/app_colors.dart';

class ManageDoors extends StatelessWidget {
  const ManageDoors({super.key});

  @override
  Widget build(BuildContext context) {
    final args = Get.arguments;
    return Scaffold(
      appBar: appBar(
        'Manage Access',
        const Icon(
          Icons.arrow_back,
          color: Colors.white70,
          size: 30,
        ),
        const Icon(
          Icons.search,
          color: Colors.white70,
          size: 30,
        ),
        PreferredSize(
          preferredSize: const Size(25, 25),
          child: Container(),
        ),
      ),
      bottomNavigationBar: navBar(0),
      body: Column(
        mainAxisAlignment: MainAxisAlignment.start,
        children: [
          const SizedBox(height: 10),
          Text(
            args['doorName'],
            style: TextStyle(
              fontSize: 25,
              fontWeight: FontWeight.bold,
              color: Colors.grey[700],
            ),
          ),
          Expanded(
            child: Container(
              padding: const EdgeInsets.only(top: 10, left: 10, right: 10),
              child: ListView(
                scrollDirection: Axis.vertical,
                children: [
                  oCard(
                    "View Qr",
                    Icon(
                      Icons.qr_code_scanner_sharp,
                      size: 70,
                      color: activeNavIconColor.withAlpha(170),
                    ),
                    args['doorId'],
                    args['doorName'],
                    context,
                  ),
                  const SizedBox(height: 20),
                  oCard(
                    "View Pin",
                    Icon(
                      Icons.pin_outlined,
                      size: 70,
                      color: activeNavIconColor.withAlpha(170),
                    ),
                    args['doorId'],
                    args['doorName'],
                    context,
                  ),
                  const SizedBox(height: 20),
                  oCard(
                    "Access",
                    Icon(
                      Icons.lock_open,
                      size: 70,
                      color: activeNavIconColor.withAlpha(170),
                    ),
                    args['doorId'],
                    args['doorName'],
                    context,
                  ),
                ],
              ),
            ),
          ),
        ],
      ),
    );
  }
}
