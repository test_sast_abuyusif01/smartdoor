// ignore_for_file: unused_local_variable

import 'package:flutter/material.dart';
import 'package:get/get.dart';

import '../../../../components/Doors/ManageAccess/AllUsers/view_all_users.dart';
import '../../../../components/all_screens/app_bar.dart';
import '../../../../components/all_screens/bottom_nav.dart';
import '../../../../controllers/Navigation/tab_view_controller.dart';

class AllCurrentAccess extends StatelessWidget {
  const AllCurrentAccess({super.key});

  @override
  @override
  Widget build(BuildContext context) {
    final Tabs tabs = Get.put(Tabs());
    return Scaffold(
        appBar: appBar(
          'Manage Access',
          const Icon(
            Icons.arrow_back,
            color: Colors.white70,
            size: 30,
          ),
          const Icon(
            Icons.search,
            color: Colors.white70,
            size: 30,
          ),
          PreferredSize(
            preferredSize: const Size(25, 25),
            child: Container(),
          ),
        ),
        bottomNavigationBar: navBar(0),
        body: Container(
          child: doorList(
            imageUrl: 'https://source.unsplash.com/user/c_v_r/100x100', // this gonna be the image of the user
            title: 'Abubakar Abubakar Yusif - 1821881',
            subtitle: 'KICT LV04 D \n10:30 - 11:30 (M-W)',
            route: '/allCurrentAccess',
            icon: const Icon(
              Icons.vpn_key_outlined,
              size: 35.0,
            ),
          ),
        ));
  }
}
