import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:smartdoor/constants/app_colors.dart';

import '../../controllers/StartUp/startup_controller.dart';

class StartupPage extends StatelessWidget {
  final _startupController = Get.put(StartupController());

  StartupPage({super.key});

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: const Color(0xFFE8F9F8),
      body: Center(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            const SizedBox(height: 50),
            const Hero(
              tag: 'title',
              child: Center(
                child: Text(
                  'Welcome to IIUM Smart Door System',
                  textAlign: TextAlign.center,
                  style: TextStyle(
                    fontSize: 24,
                    fontWeight: FontWeight.bold,
                    color: Colors.black,
                  ),
                ),
              ),
            ),
            const SizedBox(height: 20),
            Padding(
              padding: const EdgeInsets.all(20),
              child: Image.asset(
                'assets/images/logo.png',
                fit: BoxFit.contain,
              ),
            ),
            const SizedBox(height: 50),
            SizedBox(
              width: 350,
              height: 50,
              child: ElevatedButton(
                onPressed: _startupController.isLoginLoading.value ? null : () => _startupController.handleLoginButtonPressed(),
                style: ButtonStyle(
                  backgroundColor: MaterialStateProperty.all(activeNavIconColor),
                  padding: MaterialStateProperty.all(
                    const EdgeInsets.symmetric(vertical: 8.0, horizontal: 20),
                  ),
                  shape: MaterialStateProperty.all(
                    RoundedRectangleBorder(
                      borderRadius: BorderRadius.circular(10.0),
                    ),
                  ),
                ),
                child: Obx(() {
                  if (_startupController.isLoginLoading.value) {
                    return const SizedBox(
                      width: 25,
                      height: 25,
                      child: CircularProgressIndicator(
                        color: Colors.white,
                      ),
                    );
                  } else {
                    return const Text(
                      'Login',
                      style: TextStyle(fontSize: 20, color: Colors.white),
                    );
                  }
                }),
              ),
            ),
            const SizedBox(height: 15),
            SizedBox(
              width: 350,
              height: 50,
              child: ElevatedButton(
                onPressed: _startupController.isRegistrationLoading.value ? null : () => _startupController.handleRegisterButtonPressed(),
                style: ButtonStyle(
                  backgroundColor: MaterialStateProperty.all(Colors.white),
                  padding: MaterialStateProperty.all(
                    const EdgeInsets.symmetric(vertical: 10.0, horizontal: 20),
                  ),
                  shape: MaterialStateProperty.all(
                    RoundedRectangleBorder(
                      borderRadius: BorderRadius.circular(10.0),
                    ),
                  ),
                  side: MaterialStateProperty.all(
                    const BorderSide(
                      color: inactiveNavIconColor,
                      width: 1,
                    ),
                  ),
                ),
                child: Obx(() {
                  if (_startupController.isRegistrationLoading.value) {
                    return const SizedBox(
                      width: 25,
                      height: 25,
                      child: CircularProgressIndicator(
                        color: activeNavIconColor,
                      ),
                    );
                  } else {
                    return const Text(
                      'Register',
                      style: TextStyle(fontSize: 20),
                    );
                  }
                }),
              ),
            ),
            const Spacer(),
            const Text(
              'Developed and maintained by ......... under KICT',
              style: TextStyle(color: Colors.grey),
            ),
            const SizedBox(height: 20),
          ],
        ),
      ),
    );
  }
}
