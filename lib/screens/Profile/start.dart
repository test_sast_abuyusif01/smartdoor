import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:smartdoor/constants/app_colors.dart';
import '../../components/all_screens/app_bar.dart';
import '../../components/all_screens/bottom_nav.dart';
import '../../controllers/Profile/profile_controller.dart';

class Profile extends StatelessWidget {
  final profileController = Get.put(ProfileController());

  Profile({super.key});

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: appBar(
        'User Profile',
        const Icon(
          Icons.arrow_back,
          color: Colors.white70,
          size: 30,
        ),
        Icon(
          Icons.search,
          color: activeNavIconColor.withAlpha(170),
          size: 30,
        ),
        PreferredSize(
          preferredSize: const Size(25, 25),
          child: Container(),
        ),
        visibility: false,
      ),
      bottomNavigationBar: navBar(3),
      body: Container(
        decoration: BoxDecoration(
          gradient: LinearGradient(
            begin: Alignment.topLeft,
            end: Alignment.bottomRight,
            colors: [
              activeNavIconColor.withAlpha(170),
              activeNavIconColor.withAlpha(200),
              activeNavIconColor.withAlpha(170),
            ],
          ),
        ),
        child: Container(
          padding: const EdgeInsets.all(20.0),
          child: Column(
            mainAxisAlignment: MainAxisAlignment.start,
            crossAxisAlignment: CrossAxisAlignment.stretch,
            children: [
              const SizedBox(height: 32.0),
              const CircleAvatar(
                radius: 64.0,
                backgroundColor: Colors.white,
                child: Text(
                  'J',
                  style: TextStyle(
                    fontSize: 48.0,
                    color: activeNavIconColor,
                  ),
                ),
              ),
              const SizedBox(height: 40.0),
              Obx(
                () => TextFormField(
                  readOnly: !profileController.isEditing.value,
                  controller: profileController.emailController,
                  decoration: InputDecoration(
                    hintText: "Email",
                    prefixIcon: const Icon(
                      Icons.email,
                      color: Colors.white,
                    ),
                    fillColor: Colors.white.withOpacity(0.3),
                    filled: true,
                    enabledBorder: OutlineInputBorder(
                      borderSide: BorderSide.none,
                      borderRadius: BorderRadius.circular(10.0),
                    ),
                    focusedBorder: OutlineInputBorder(
                      borderSide: BorderSide.none,
                      borderRadius: BorderRadius.circular(10.0),
                    ),
                  ),
                ),
              ),
              const SizedBox(height: 20.0),
              Obx(
                () => TextFormField(
                  readOnly: !profileController.isEditing.value,
                  controller: profileController.passwordController,
                  obscureText: true,
                  decoration: InputDecoration(
                    hintText: "Password",
                    prefixIcon: const Icon(
                      Icons.lock,
                      color: Colors.white,
                    ),
                    fillColor: Colors.white.withOpacity(0.3),
                    filled: true,
                    enabledBorder: OutlineInputBorder(
                      borderSide: BorderSide.none,
                      borderRadius: BorderRadius.circular(10.0),
                    ),
                    focusedBorder: OutlineInputBorder(
                      borderSide: BorderSide.none,
                      borderRadius: BorderRadius.circular(10.0),
                    ),
                  ),
                ),
              ),
              const SizedBox(height: 20.0),
              Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  Obx(
                    () => ElevatedButton(
                      onPressed: () {
                        profileController.toggleEditing();
                      },
                      style: ButtonStyle(
                        backgroundColor: MaterialStateProperty.all(Colors.white),
                        padding: MaterialStateProperty.all(
                          const EdgeInsets.symmetric(vertical: 20.0, horizontal: 20),
                        ),
                        shape: MaterialStateProperty.all(
                          RoundedRectangleBorder(
                            borderRadius: BorderRadius.circular(10.0),
                          ),
                        ),
                      ),
                      child: Text(profileController.isEditing.value ? 'Cancel' : 'Edit Profile'),
                    ),
                  ),
                  Obx(
                    () => Visibility(
                      visible: profileController.isEditing.value,
                      child: ElevatedButton(
                        onPressed: () {
                          profileController.toggleEditing();
                          profileController.updateUserInfo(context);
                        },
                        style: ButtonStyle(
                          backgroundColor: MaterialStateProperty.all(Colors.white),
                          padding: MaterialStateProperty.all(
                            const EdgeInsets.symmetric(vertical: 20.0, horizontal: 20),
                          ),
                          shape: MaterialStateProperty.all(
                            RoundedRectangleBorder(
                              borderRadius: BorderRadius.circular(10.0),
                            ),
                          ),
                        ),
                        child: const Text('Save'),
                      ),
                    ),
                  ),
                ],
              ),
            ],
          ),
        ),
      ),
    );
  }
}
