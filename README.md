# SmartDoor MOBILEE APP STANDALONE INSTALLATION

Mobile app for smart door system - IIUM

## Table of Contents

- [SmartDoor MOBILEE APP STANDALONE INSTALLATION](#smartdoor-mobilee-app-standalone-installation)
  - [Table of Contents](#table-of-contents)
  - [Prerequisites](#prerequisites)
  - [Installation - Flutter App- Standalone](#installation---flutter-app)
  - [Installation - Backend API - Standalone](#installation---backend)
    - [Configurating Backend API](#configuration-backend-api)
  - [Installation - MQTT - Standalone](#installation---mqtt)
    - [Usage MQTT Broker](#usage-mqtt-broker)
- [SmartDoor MOBILEE APP DOCKER INSTALLATION](#smartdoor-mobilee-app-docker-installation)




## Prerequisites

- Node.js and npm
- Flutter SDK (for mobile app)
- Postgres
- Postman (for testing)
- Git (for cloning)
- Docker
- Docker-compose
- Emulator
- Vscode (for IDE + configs)

## Installation - Flutter App

1. Clone this repository to your local machine:

   ```bash
   git clone https://github.com/abuyusif01/smartdoor.git $HOME/smartdoor
   ```

2. Navigate to the project directory:

   ```bash
   cd smartdoor
   ```

3. Install the project dependencies:

   ```bash
   flutter pub get
   flutter pub upgrade
   ```

4. Run the project:

   ```bash
    flutter run
    ```

## Installation - Backend
This will install the backend API server for the smart door system locally. 
1. Navigate to /lib/backend directory:

   ```bash
   cd lib/backend/sdak-api
   ```

   sample output of tree command:
   ```txt
    ├── api
    │   ├── config
    │   │   └── authConfig.js
    │   ├── controllers
    │   │   ├── adminRoutesController.js
    │   │   ├── authRoutesController.js
    │   │   ├── cert
    │   │   │   └── ca.crt
    │   │   ├── config
    │   │   │   └── config.json
    │   │   ├── modRoutesController.js
    │   │   ├── userRoutesController.js
    │   │   └── utils
    │   │       └── validator.js
    │   ├── middleware
    │   │   ├── authJwt.js
    │   │   ├── index.js
    │   │   └── verifySignUp.js
    │   └── routes
    │       ├── adminRoutes.js
    │       ├── authRoutes.js
    │       ├── modRoutes.js
    │       └── userRoutes.js
    ├── config
    │   ├── config.js
    │   └── config.json
    ├── Dockerfile
    ├── index.js
    ├── migrations
    ├── models
    │   ├── door.js
    │   ├── index.js
    │   ├── passcode.js
    │   ├── permission.js
    │   └── user.js
    ├── package.json
    ├── package-lock.json
    ├── req.http
    ├── seeders
    ├── ssl
    │   ├── ca.crt
    │   ├── localhost-key.pem
    │   └── localhost.pem
    ├── test
    │   ├── test_db.py
    │   └── test_endpoints.py
    └── test_paho.js

    15 directories, 33 files
    ```

2. Install the project dependencies:

   ```bash
   npm install
   ```

### Configuration Backend API

1. Navigate to /config/config.js file in the root of the project.

2. Configure the following environment variables in the `config.js file` file:

   ```js
        module.exports = {
            development: {
                dialect: 'postgres',
                host: '__hostname__',
                username: '__username__',
                password: '__passwd__',
                database: '__dbname__',
                logging: false
            }
        }
    ```

4. repeat above steps for /api/config/config.json file

5. generate a random and secure key
   ```bash
    node -e "console.log(require('crypto').randomBytes(256).toString('base64'));"
    ```
6. copy the generated key and paste it in /api/config/authConfig file
7. set up ssl in the /ssl directory

## Installation - MQTT

This will install the MQTT broker for the smart door system locally. depends on the distro you are using (am on arch), you can install the broker using the following commands:

1. install mosquitto broker
   ```bash
    pacman -S mosquitto
    ```
2. run mosquitto with ssl-config
   ```bash
    mosquitto -c $HOME/smartdoor/lib/backend/container/mosquitto-ssl.conf
   ``` 

### Usage MQTT Broker

 1. subscribe to a topic
    ```bash
    mosquitto_sub -h localhost -p 8883 --cafile $HOME/smartdoor/lib/backend/container/cert/ca.crt -t "topic" --username "username" --pw "1234"
    ```
   2. publish to a topic
      ```bash
      mosquitto_pub -h localhost -p 8883 --cafile $HOME/smartdoor/lib/backend/container/cert/ca.crt -t "topic" -m "message" --username "username" --pw "1234"
      ```


# SmartDoor MOBILEE APP DOCKER INSTALLATION

If you chose to run the backend in a docker container, you must satisfy the [pre-requisites](#prerequisites) and [flutter-app](#installation---flutter-app) section and follow the steps below:

1. Navigate to the project directory:

   ```bash
   cd $HOME/smartdoor/lib/backend/container
   ```
2. Configure `.env` file to be use in the docker-compose file:
   
      ```js
      POSTGRES_USER='__user__'
      POSTGRES_PASSWORD='__passwd__'
      POSTGRES_DB='smartdoor'
   ``` 

3. Create a network for the containers to communicate with each other:

   ```bash
   docker network create smartdoor-network
   ```
4. Follow the steps in [api-config](#configuration-backend-api) and make sure all db creds are exported as shown in the section.
4. Build the compose file and run it:

   ```bash
   docker-compose build
   ```
5. Run the project:

   ```bash
   docker-compose up
   ```
Above command will run the backend API and the MQTT broker in two separate containers. Exposing port 8883, 8081, 5432

- 8883: MQTT broker
- 8081: Backend API
- 5432: Postgres
